// 컴파일 언어에서 array(요소가 바뀌면 안되는 값)은 요소의 갯수를 수정할 수 없다.
// => 이를 해소하기 위해 list를 사용
// json(javaScript Object Notation)


// javascript는 객체
let test = {
    name: "hong",
    age: 12,
    hobby: ['computer', 'sleep'],
    job: {
        count:2,
        list: [
            {
                name: '강사',
                company: 'line art'
            },
            {
                name: '편순이',
                company: 'GS25'
            }
        ]
    }
}

test['name']
test['job']["list"][1]['name']
let test2 = []

//편의점 키오스크
// 딸기우유, 초코우유, 가나초콜렛, 맛동산, 자갈치