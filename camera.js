// Init camera
function camInit(stream){
    let cameraView = document.getElementById('cameraview');
    // 객체의 메모리 주소를 가지고 오는 구문 (ref)
    cameraView.srcObject = stream;
    // stream 데이터를 계속 플레이
    cameraView.play();
}

function camInitFailed(error){
    console.log("get camera permission failed : ", error)
}

// Main init

function mainInit(){
    //Check navigator media device available
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        // Navigator mediaDevices not supported
        alert("Media devices not supported")
        return;
        // return 나오면 메서드의 동작을 끝내겠다
    }

    navigator.mediaDevices.getUserMedia({video: true})
        .then(camInit)
        .catch(camInitFailed);
    // .then, catch => promise
    // 캠 켜는거 성공했으면 camInit 실행
    // 캠 키는데 실패했으면 console.log를 남긴다
    // 구조를 모르니까 오버로딩, 오버라이딩 불가
}