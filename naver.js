document.addEventListener('DOMContentLoaded', function() {
    // 이 코드는 DOMContentLoaded 이벤트가 발생한 후 실행됩니다.
    // 따라서 이벤트 핸들러 내에서 안전하게 DOM 요소에 접근할 수 있습니다.
    let buttons = document.querySelectorAll('.yum-navbar-btn');
    let activeButton = null;
    buttons.forEach(function(button) {
        button.addEventListener('click', function() {
            // 현재 클릭된 버튼에 'clicked' 클래스 추가
            button.classList.add('active');

            // 이전에 활성화되었던 버튼이 있을 경우 해당 버튼의 'active' 클래스 제거
            if (activeButton && activeButton !== button) {
                activeButton.classList.remove('active');
            }

            // 현재 클릭된 버튼을 활성화된 버튼으로 설정
            activeButton = button;
        });
    });

    // button.addEventListener('click', function() {
    //     console.log('clicked');
    //     // 클래스 'active'를 추가/제거하여 상태를 토글합니다.
    //     button.classList.toggle('active');
    // });
});